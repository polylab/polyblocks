class Polyblock extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.currentHtml = this.template;
        this.config = JSON.parse(this.dataset.config);
        this.parseDataContainers();

        this.innerHTML = this.currentHtml;
    }

    parseDataContainers() {
        this.currentHtml = this.template;
        for(let propertyName in this.config) {
            this.currentHtml = this.currentHtml.replace('{{'+propertyName+'}}', this.config[propertyName]);
        };
    }

    updateData(propertyName, value) {
        this.config[propertyName] = value;
        this.checkForDataContainersToFill();
    }
}